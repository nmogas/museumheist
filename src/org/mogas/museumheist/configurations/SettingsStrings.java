package org.mogas.museumheist.configurations;

/**
 * Created by nuno.silva on 18/02/2015.
 */
public enum SettingsStrings {
    /* Number of rooms in the museum */
    NUMBER_ROOMS,

    /* Number of  thieves including master thief */
    NUMBER_THIEVES,

    /* Maximum number of thieves per group */
    NUMBER_ELEMENTS_PER_GROUP,

    /* Maximum distance between two thieves while crawling */
    MAX_DISTANCE_BETWEEN_THIEVES,

    /* Maximum strength a thief can have */
    MAX_THIEF_STRENGTH,

    /* Minimum number of canvas a room can have */
    MIN_NUMBER_CANVAS,

    /* Maximum number of canvas a room can have */
    MAX_NUMBER_CANVAS,

    /* Minimum distance to a room */
    MIN_ROOM_DISTANCE,

    /* Maximum distance to a room */
    MAX_ROOM_DISTANCE,

    /* Is Debug */
    DEBUG
}
