package org.mogas.museumheist.configurations;

import java.io.*;
import java.util.*;

/**
 * Created by nuno.silva on 18/02/2015.
 */
public class ApplicationSettings {

    /* Settings list: map the name of the setting to an object (with a class). */
    private static HashMap<String, String> settings;

    private static Properties properties;

    private static String separator = System.getProperty("file.separator");

    private static String settingsFilePath = "configurations";

    private static String settingsFileName = "settings.ini";

    private static volatile ApplicationSettings instance = null;

    /**
     * singleton instantiation thread safe
     * @return
     */
    public static ApplicationSettings getInstance(){
        if (instance == null){
            synchronized (ApplicationSettings.class){
                if (instance == null){
                    instance = new ApplicationSettings();
                }
            }
        }
        return instance;
    }

    private ApplicationSettings(){
        settings = new HashMap<String, String>();
        properties = new Properties();

        //check if file exits and load it to properties
        if (new File(settingsFilePath, settingsFileName).exists()){
            try {
                properties.load(new FileInputStream((new File(settingsFilePath, settingsFileName))));

                //load file settings
                System.out.println("Loading settings file...");
                for(Object key : properties.keySet()){
                    settings.put((String) key, properties.getProperty((String) key));
                }
                System.out.println("Loading settings complete!\n");

            } catch (IOException e) {
                System.out.println("Error loading settings");
                e.printStackTrace();
            }
        }else{
            //create default settings
            createDefaultSettingsFile();
        }

         /* Verify if settings is current version*/
        verifySettingsFile();
    }

    /**
     * Default settings
     * @return List of the default settings
     */
    private List<Setting> getDefaultSettings() {
        List<Setting> defaultSettings = new ArrayList<Setting>();

        /* Number of rooms in the museum */
        defaultSettings.add(new Setting(SettingsStrings.NUMBER_ROOMS.toString(), 5, Integer.class));

        /* Number of  thieves including master thief */
        defaultSettings.add(new Setting(SettingsStrings.NUMBER_THIEVES.toString(), 7, Integer.class));

        /* Maximum number of thieves per group */
        defaultSettings.add(new Setting(SettingsStrings.NUMBER_ELEMENTS_PER_GROUP.toString(), 3, Integer.class));

        /* Maximum distance between two thieves while crawling */
        defaultSettings.add(new Setting(SettingsStrings.MAX_DISTANCE_BETWEEN_THIEVES.toString(), 2, Integer.class));

        /* Maximum strength a thief can have */
        defaultSettings.add(new Setting(SettingsStrings.MAX_THIEF_STRENGTH.toString(), 3, Integer.class));

        /* Minimum number of canvas a room can have */
        defaultSettings.add(new Setting(SettingsStrings.MIN_NUMBER_CANVAS.toString(), 5, Integer.class));

       /* Maximum number of canvas a room can have */
        defaultSettings.add(new Setting(SettingsStrings.MAX_NUMBER_CANVAS.toString(), 20, Integer.class));

        /* Maximum distance to a room */
        defaultSettings.add(new Setting(SettingsStrings.MIN_ROOM_DISTANCE.toString(), 8, Integer.class));

        /* Maximum distance to a room */
        defaultSettings.add(new Setting(SettingsStrings.MAX_ROOM_DISTANCE.toString(), 20, Integer.class));

        /* Enable/Disable debug */
        defaultSettings.add(new Setting(SettingsStrings.DEBUG.toString(), Boolean.FALSE, Boolean.class));

        return defaultSettings;
    }

    private void createDefaultSettingsFile() {
        System.out.println("Creating new settings file...");

        List<Setting> defaultSettings = getDefaultSettings();

        //check if directory exists, if not create it
        File dir = new File(settingsFilePath);
        if (!dir.exists()){
            dir.mkdir();
        }
        File settingsFile = new File(settingsFilePath, settingsFileName);
        try {
            settingsFile.createNewFile();

            for (Iterator<Setting> it = defaultSettings.iterator(); it.hasNext();) {
                Setting setting = it.next();
                setProperty(setting.propertyName, setting.value, setting.type);
            }
        } catch (IOException ex) {
            //TODO: exceptions...
            System.out.println("Fail creating settings file !!!");
            ex.printStackTrace();
        }

        System.out.println("Creating new settings file complete!");
    }

    /**
     * Verify that all settings needed and if there are not create them
     */
    private void verifySettingsFile() {
        System.out.println("Verifying the settings file...");

        List<Setting> defaultSettings = getDefaultSettings();

        for (Iterator<Setting> it = defaultSettings.iterator(); it.hasNext();) {
            Setting setting = it.next();

            if ( !settings.containsKey(setting.propertyName) ) {
                setProperty(setting.propertyName, setting.value, setting.type);
            }
        }
        System.out.println("Verifying the settings file complete!\n");
    }

    public void setProperty(String propertieName, Object value, Class type){
        String strValue = null;

        if (type == String.class){
            strValue = (String) value;
        }
        if (type == Integer.class){
            strValue = Integer.toString((Integer) value);
        }
        if (type == Boolean.class){
            strValue = Boolean.toString((Boolean) value);
        }
        if (type == Double.class){
            strValue = Double.toString((Double) value);
        }

        settings.put(propertieName, strValue);
        properties.put(propertieName, strValue);

        try {
            FileOutputStream out = new FileOutputStream(new File(settingsFilePath, settingsFileName));
            properties.store(out, "/* properties updated on: */");
            out.flush();
            out.close();
        } catch (Exception e) {
            //TODO: exceptions
            e.printStackTrace();
        }
    }

    public Object getProperty(String propertieName, Class returnType){
        if (!settings.containsKey(propertieName)){
            return null;
        }
        if (returnType == String.class){
            return String.valueOf(settings.get(propertieName));
        }
        if (returnType == Integer.class){
            return Integer.valueOf(settings.get(propertieName));
        }
        if (returnType == Boolean.class){
            return Boolean.parseBoolean(settings.get(propertieName));
        }
        if (returnType == Double.class){
            return Double.parseDouble(settings.get(propertieName));
        }

        return null;
    }

    /**
     * To save a setting in vector
     */
    private class Setting{
        String propertyName;
        Object value;
        Class type;

        public Setting(String propertyName, Object value, Class type) {
            this.propertyName = propertyName;
            this.value = value;
            this.type = type;
        }
    }
}
