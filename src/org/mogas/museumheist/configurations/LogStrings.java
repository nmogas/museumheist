package org.mogas.museumheist.configurations;

/**
 * Created by nuno.silva on 27/02/2015.
 */
public class LogStrings {

    public static String HEADER = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tHeist to the museum - Description of the internal state\n\n";

    public static String MASTER_THIEF = "MASTER THIEF";

    public static String THIEF = "THIEF";

    public static String ROOM = "ROOM";

    public static String STATE = "STATE";

    public static String STRENGTH = "Str";

    public static String ON_PARTY = "OnP";

    public static String TARGET_ROOM = "RId";

    public static String POSITION = "Pos";

    public static String HAS_CANVAS = "Hc";

    public static String ROOM_N_CANVAS = "Nc";

    public static String ROOM_DISTANCE = "DIST";

    public static String TOTAL_CANVAS = "TCanvas";
}
