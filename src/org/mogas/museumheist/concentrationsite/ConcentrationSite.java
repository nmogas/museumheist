package org.mogas.museumheist.concentrationsite;

import org.mogas.museumheist.configurations.ApplicationSettings;
import org.mogas.museumheist.configurations.SettingsStrings;
import org.mogas.museumheist.logger.ActionMessage;
import org.mogas.museumheist.logger.LogMessage;
import org.mogas.museumheist.logger.LogMessageType;
import org.mogas.museumheist.logger.Logger;
import org.mogas.museumheist.structures.AssaultParty;
import org.mogas.museumheist.structures.ThiefDetails;
import sun.util.resources.ar.CurrencyNames_ar_TN;

import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created by nuno.silva on 18/02/2015.
 */
public class ConcentrationSite implements IConcentrationMasterThief, IConcentrationOrdinaryThief {

    //holds log reference
    private Logger eventLog;

    private final int N_PARTY_ELEMENTS = (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.NUMBER_ELEMENTS_PER_GROUP.toString(), Integer.class);

    private Queue<ThiefDetails> thievesPresence;

    public ConcentrationSite(Logger eventLog) {
        this.eventLog = eventLog;

        thievesPresence = new PriorityBlockingQueue<>();
    }

    @Override
    public synchronized AssaultParty prepareAssaultParty(int roomId) throws InterruptedException {
        //create a new list of thieves
        List<ThiefDetails> thievesList = new ArrayList<>();

        //assemble team
        while(thievesList.size() != N_PARTY_ELEMENTS){
            if(!thievesPresence.isEmpty()){
                //remove thief from queue
                ThiefDetails newThief = thievesPresence.poll();
                //update thief status
                newThief.setStatus(ThiefDetails.ThiefStatus.UNAVAILABLE);
                //add thief to the party
                thievesList.add(newThief);
                //notify thief that he is now on a party - not ready to go yet

                notifyAll();
            }else{
                //wait for thieves to arrive
                wait();
            }
        }
        return new AssaultParty(roomId, thievesList);
    }


    @Override
    public synchronized void sumUpResults(int totalCanvas) throws InterruptedException {

        int totalThieves = ((Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.NUMBER_THIEVES.toString(), Integer.class) - 1);
        while(totalThieves != 0){
            if (!thievesPresence.isEmpty()) {
                //remove thief from presence
                ThiefDetails thief = thievesPresence.poll();
                //update thief status to not needed - operation came to an end
                thief.setStatus(ThiefDetails.ThiefStatus.NOT_NEEDED);
                //update presence
                totalThieves--;
                //notify thief that he his no longer needed
                notifyAll();
            }else{
                //wait for thieves arrival
                wait();
            }
        }

        System.out.println("Total stolen canvases: " + totalCanvas);
    }

    @Override
    public synchronized boolean amINeeded(ThiefDetails thief) throws InterruptedException {

        //thief was on a mission, could never be in the presence list!!!!
        assert !thievesPresence.contains(thief);

        //mark thief as available
        thief.setStatus(ThiefDetails.ThiefStatus.AVAILABLE);

        //reset thief positions
        thief.resetPosition();

        //add to the queue of available thieves
        thievesPresence.add(thief);

        //notify master thief of his presence
        notifyAll();

        //wait for master thief call
        while(thief.getStatus() == ThiefDetails.ThiefStatus.AVAILABLE){
            wait();
        }

        //check his status to know if the operation has come to an end
        if(thief.getStatus() == ThiefDetails.ThiefStatus.NOT_NEEDED){
            return false;
        }

        return true;
    }

}
