package org.mogas.museumheist.concentrationsite;

import org.mogas.museumheist.structures.ThiefDetails;

/**
 * Created by nuno.silva on 18/02/2015.
 */
public interface IConcentrationOrdinaryThief {

    /**
     * Thief waits for orders from the master thief
     * @param thief details
     * @return TRUE, if he will join an assault party, FALSE is the operation came to an end
     * @throws InterruptedException
     */
    boolean amINeeded(ThiefDetails thief) throws InterruptedException;
}
