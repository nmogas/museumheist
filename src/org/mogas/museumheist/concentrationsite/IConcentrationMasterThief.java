package org.mogas.museumheist.concentrationsite;

import org.mogas.museumheist.structures.AssaultParty;
import org.mogas.museumheist.museum.Room;

/**
 * Created by nuno.silva on 18/02/2015.
 */
public interface IConcentrationMasterThief {

    /**
     * Creates a new assault party to target a specific room - gets thieves waiting for orders
     * @param roomId Id of the room to be assaulted
     * @return Assault party that was deployed
     * @throws InterruptedException
     */
    AssaultParty prepareAssaultParty(int roomId) throws InterruptedException;


    /**
     * Sum up the results of the operation
     * @param totalCanvas total number of canvasses stolen  from the museum
     * @throws InterruptedException
     */
    void sumUpResults(int totalCanvas) throws InterruptedException;
}
