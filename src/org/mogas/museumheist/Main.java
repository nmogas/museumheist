package org.mogas.museumheist;

import org.mogas.museumheist.configurations.ApplicationSettings;

/**
 * Created by nuno.silva on 17/02/2015.
 */
public class Main {

    public static void main(String[] args) {

        //load application default settings
        ApplicationSettings.getInstance();

        int n_threads = 1;
        for (int i = 0; i < n_threads; i++) {
            //starts the simulation
            new Thread(new Heist()).start();
        }

    }
}
