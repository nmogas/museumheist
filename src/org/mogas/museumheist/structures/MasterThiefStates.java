package org.mogas.museumheist.structures;

/**
 * Created by nuno.silva on 27/02/2015.
 */
public enum  MasterThiefStates {

    PLANNING_THE_HEIST,

    DECIDING_WHAT_TO_DO,

    ASSEMBLING_A_GROUP,

    WAITING_FOR_GROUP_ARRIVAL,

    PRESENTING_THE_REPORT

}
