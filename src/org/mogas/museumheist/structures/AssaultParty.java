package org.mogas.museumheist.structures;

import org.mogas.museumheist.configurations.ApplicationSettings;
import org.mogas.museumheist.configurations.SettingsStrings;

import java.util.List;

/**
 * Created by nuno.silva on 18/02/2015.
 */
public class AssaultParty implements Comparable{

    private final Object partyLock = new Object();

    /**
     * Maximum number of thieves per assault party
     */
    private final int N_THIEVES_PER_GROUP = (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.NUMBER_ELEMENTS_PER_GROUP.toString(), Integer.class);


    /**
     * Target room id of this assault party
     */
    private int roomId;

    /**
     * Ids of the thieves present in this assault party
     */
    private List<ThiefDetails> thiefList;

    public AssaultParty(int roomId, List<ThiefDetails> thiefList) {
        this.roomId = roomId;

        this.thiefList = thiefList;
    }

    public void deleteThief(ThiefDetails thief) {
        synchronized (partyLock) {
            for (int i = 0; i < thiefList.size(); i++) {
                if (thiefList.get(i).getThiefId() == thief.getThiefId()) {
                    thiefList.remove(i);
                }
            }
        }
    }

    public List<ThiefDetails> getThieves() {
        synchronized (partyLock) {
            return thiefList;
        }
    }

    public boolean thiefExists(ThiefDetails inputThief) {
        synchronized (partyLock){
            for(ThiefDetails thief : thiefList){
                if(thief.getThiefId() == inputThief.getThiefId()){
                    return true;
                }
            }
            return false;
        }
    }

    public boolean isFull() {
        synchronized (partyLock) {
            return thiefList.size() == N_THIEVES_PER_GROUP;
        }
    }

    public boolean isEmpty() {
        synchronized (partyLock) {
            return thiefList.isEmpty();
        }
    }

    public int getRoomId() {
        synchronized (partyLock) {
            return roomId;
        }
    }

    @Override
    public int compareTo(Object o) {
        return this.roomId == ((AssaultParty) o).getRoomId() ? 1 : 0;
    }

    public boolean containsThief(Object o){
        for(ThiefDetails thief : thiefList){
            for(ThiefDetails thiefInput : (List<ThiefDetails>) o){
                if (thief.getThiefId() == thiefInput.getThiefId()){
                    return true;
                }
            }
        }
        return false;
    }

}