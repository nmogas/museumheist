package org.mogas.museumheist.structures;

import org.mogas.museumheist.configurations.ApplicationSettings;
import org.mogas.museumheist.configurations.SettingsStrings;
import org.mogas.museumheist.logger.LogMessage;
import org.mogas.museumheist.logger.LogMessageType;
import org.mogas.museumheist.logger.Logger;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by nuno.silva on 25/02/2015.
 */
public class MuseumCrawlParty {

    /**
     * Holds log reference
     */
    private Logger eventLog;

    private boolean isPartyReady;

    private final int MAX_DISTANCE_BETWEEN_THIEVES = (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.MAX_DISTANCE_BETWEEN_THIEVES.toString(), Integer.class);

    private final int N_ELEMENTS_PER_GROUP = (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.NUMBER_ELEMENTS_PER_GROUP.toString(), Integer.class);

    private List<ThiefDetails> tList;

    private int roomDistance;

    private boolean isFirst;

    private int nextThiefToCrawl;

    public MuseumCrawlParty(Logger eventLog, int roomDistance) {
        this.eventLog = eventLog;
        this.roomDistance = roomDistance;
        this.tList = new ArrayList<>();
    }

    public boolean isDeployed() {
        return isPartyReady;
    }

    public void setFirst(boolean isFirst) {
        this.isFirst = isFirst;
    }

    public synchronized void addThief(ThiefDetails thief) {
        if (tList.isEmpty()) {
            //this thief is the first to crawl
            nextThiefToCrawl = thief.getThiefId();
        }
        if (!tList.stream().anyMatch(x -> x.getThiefId() == thief.getThiefId())) {
            tList.add(thief);
            if (tList.size() == N_ELEMENTS_PER_GROUP) {
                this.isPartyReady = true;
            }
        }
    }

    public  synchronized void removeFromParty(ThiefDetails thief){
        tList.remove(thief);
        if (tList.isEmpty()){
            tList = new ArrayList<>();
            //for every new party going for this room
            this.isPartyReady = false;
            //mark as needs a first crawl
            this.isFirst = true;
        }

    }

    public synchronized void setNextThiefToCrawl(ThiefDetails currentThief) {
        if (isFirst == true) {
            //return the first thief joining the party
            return;
        }

        int currentPosition = currentThief.getCurrentPosition();

        Map<Integer, Integer> tPositions = new HashMap<>();
        //get all positions besides his
        tList.stream().filter(t -> t.getThiefId() != currentThief.getThiefId())
                .forEach(x -> tPositions.put(x.getThiefId(), x.getCurrentPosition()));

        if (tPositions.isEmpty()) {
            //I am the only one crawling!
            nextThiefToCrawl = currentThief.getThiefId();
            return;
        }

        int furthestPositionThief = -1;
        int furthestDistantThiefId = -1;
        int closestBehindThiefPosition = -1;
        int closestBehindThiefId = -1;

        for (Map.Entry<Integer, Integer> entry : tPositions.entrySet()) {
            if (entry.getValue() > currentPosition) {
                if (furthestPositionThief < entry.getValue()) {
                    furthestPositionThief = entry.getValue();
                    furthestDistantThiefId = entry.getKey();
                }
            } else {
                if (closestBehindThiefPosition < entry.getValue()) {
                    closestBehindThiefPosition = entry.getValue();
                    closestBehindThiefId = entry.getKey();
                }
            }
        }

        if (closestBehindThiefPosition == -1) {
            //no one behind him - get the further one!
            nextThiefToCrawl = furthestDistantThiefId;
            return;
        }
        //get the one right behind him
        nextThiefToCrawl = closestBehindThiefId;
    }

    public int getNextThiefIdToCrawl() {
        return nextThiefToCrawl;
    }

    public synchronized boolean canCrawl(ThiefDetails thief, int jumpSize){

        int attemptPosition = thief.getCurrentPosition() + jumpSize;

        //check if position is occupied!
        if (!isPositionFree(attemptPosition)) {
            return false;
        }

        if (thief.getCurrentPosition() < roomDistance && attemptPosition > roomDistance){
            return false;
        }

        //check if crawl exceed the distance
        //crawling outwards
        if (thief.getCurrentPosition() < roomDistance) {
            //he must stop at the room!
            if (attemptPosition > roomDistance) {
                return false;
            }
        } else {
            //crawling inwards - he must stop at collection site!
            if (attemptPosition > roomDistance * 2) {
                return false;
            }
        }

        //not at the room neither collection site - check distance between thieves
        List<Integer> sourcePos = tList.stream().map(ThiefDetails::getCurrentPosition).collect(Collectors.toList());
        List<Integer> positions = new ArrayList<Integer>(sourcePos);

        //get the list of all positions
        Collections.copy(positions, sourcePos);

        for(int i = 0; i < positions.size(); i++){
            if (positions.get(i) == thief.getCurrentPosition()){
                positions.remove(i);
                positions.add(attemptPosition);
            }
        }

        //sort positions
        Collections.sort(positions);

        //check max distance rule
        for(int i = 1; i < positions.size(); i++){
            if (positions.get(i) - positions.get(i - 1)  > MAX_DISTANCE_BETWEEN_THIEVES){
                return false;
            }
        }

        //all rules are ok!
        return true;
    }

    public int getCurrentPartySize() {
        return tList.size();
    }

    private  boolean isPositionFree(int position){
        //check if the position is already occupied
        return tList.stream()
                .filter(x -> x.getCurrentPosition() == position)
                .count() == 0;
    }
}
