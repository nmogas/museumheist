package org.mogas.museumheist.structures;

/**
 * Created by nuno.silva on 27/02/2015.
 */
public enum OrdinaryThiefState {

    AM_I_NEEDED,
    WAITING_FOR_PARTY_COMPLETION,
    CRAWLING_OUTWARDS,
    STEALING_CANVAS,
    CRAWLING_INWARDS,
    DELIVERING_CANVAS
}
