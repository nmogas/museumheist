package org.mogas.museumheist.structures;

/**
 * Created by nuno.silva on 23/02/2015.
 */
public class ThiefDetails implements Comparable{

    /**
     * Unique identifier of the thief
     */
    private int thiefId;

    /**
     * Flag showing if thief is carrying a flag
     */
    private boolean hasCanvas;

    /**
     * Current status of the thief
     */
    private ThiefStatus status;


    /**
     * Thief current position
     */
    private int currentPosition;

    /**
     * Thief previous position
     */
    private int previousPosition;

    /**
     * Thief expertise
     */
    private int strength;

    public ThiefDetails(int thiefId, int strength){
        this.thiefId = thiefId;
        this.strength = strength;

        //initially all the thieves are available
        this.status = ThiefStatus.AVAILABLE;

        resetPosition();
    }

    public int getStrength() {
        return strength;
    }

    public int getThiefId() {
        return thiefId;
    }

    public ThiefStatus getStatus() {
        return status;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public int getPreviousPosition(){
        return this.previousPosition;
    }

    public void setCurrentPosition(int jump) {
        //set old position
        this.previousPosition = this.currentPosition;
        //update new position
        this.currentPosition = this.previousPosition + jump;
    }

    public void resetPosition(){
        this.currentPosition = 0;
        this.previousPosition = 0;
    }

    public boolean hasCanvas() {
        return hasCanvas;
    }

    public void setHasCanvas(boolean hasCanvas) {
        this.hasCanvas = hasCanvas;
    }

    @Override
    public int compareTo(Object o) {
        return this.thiefId == ((ThiefDetails) o).getThiefId() ? 1 : 0;
    }

    @Override
    public boolean equals(Object o){
        return this.thiefId == ((ThiefDetails) o).getThiefId();
    }

    public void setStatus(ThiefStatus status) {
        this.status = status;
    }

    public enum ThiefStatus {
        UNAVAILABLE,
        AVAILABLE,
        NOT_NEEDED,
    }
}

