package org.mogas.museumheist;

import org.mogas.museumheist.collectionsite.CollectionSite;
import org.mogas.museumheist.concentrationsite.ConcentrationSite;
import org.mogas.museumheist.configurations.ApplicationSettings;
import org.mogas.museumheist.configurations.SettingsStrings;
import org.mogas.museumheist.entities.MasterThief;
import org.mogas.museumheist.entities.Thief;
import org.mogas.museumheist.logger.ActionMessage;
import org.mogas.museumheist.logger.LogMessage;
import org.mogas.museumheist.logger.LogMessageType;
import org.mogas.museumheist.logger.Logger;
import org.mogas.museumheist.museum.Museum;
import org.mogas.museumheist.museum.Room;

/**
 * Created by nuno.silva on 18/02/2015.
 */
public class Heist implements Runnable {


    private final int N_ROOMS = (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.NUMBER_ROOMS.toString(), Integer.class);

    private final int N_THIEVES = (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.NUMBER_THIEVES.toString(), Integer.class);

    private final boolean isDebug = (Boolean) ApplicationSettings.getInstance().getProperty(SettingsStrings.DEBUG.toString(), Boolean.class);

    @Override
    public void run() {

        //space reservation for the number of rooms required
        Room[] rooms = new Room[N_ROOMS];

        //ids for the master thief - he only knows how many rooms exists - not their content
        int[] roomsIds = new int[N_ROOMS];

        //init the log
        final Logger eventLog = new Logger(N_THIEVES, N_ROOMS);
        //log example
        //eventLog.registerEvent(new LogMessage(LogMessageType.COLLECTION_SITE, -1, -1, true, ActionMessage.TAKING_REST));

        if (isDebug) {
            System.out.println("Total number of thieves:\t" + N_THIEVES);
            System.out.println("Total number of rooms:\t" + N_ROOMS);
            System.out.println();
        }

        for (int i = 0; i < N_ROOMS; i++) {
            rooms[i] = new Room(i, eventLog);
            roomsIds[i] = i;
            if (isDebug) {
                System.out.println("\tRoom " + i + " has " + rooms[i].getNCanvas() + " canvas with distance " + rooms[i].getDISTANCE());
            }
        }

        //Monitors initialization
        CollectionSite collectionSite = new CollectionSite(eventLog);
        ConcentrationSite concentrationSite = new ConcentrationSite(eventLog);
        Museum museum = new Museum(rooms, eventLog);

        /* creates and starts master thief thread */
        Thread masterThief = new Thread(new MasterThief(eventLog, concentrationSite, collectionSite, roomsIds));
        masterThief.start();


        /* creates and starts thieves threads */
        Thread[] thieves = new Thread[N_THIEVES - 1];
        for (int i = 0; i < thieves.length; i++) {
            thieves[i] = new Thread(new Thief(i, eventLog, concentrationSite, collectionSite, museum));
            thieves[i].start();
        }

        if (isDebug) {
            System.out.println("Solution started...");
        }

        //wait for thieves and master thief to finish
        try {
            for (Thread t : thieves) {
                t.join();
                System.out.println(t.getId() + " finished!");
            }
            masterThief.join();
        } catch (InterruptedException e) {
            //todo: exceptions
            e.printStackTrace();
        }

        if (isDebug) {
            System.out.println("Solution ended...");
        }
    }
}
