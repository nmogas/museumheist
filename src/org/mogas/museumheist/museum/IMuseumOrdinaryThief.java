package org.mogas.museumheist.museum;

import org.mogas.museumheist.structures.ThiefDetails;

/**
 * Created by nuno.silva on 18/02/2015.
 */
public interface IMuseumOrdinaryThief {

    /**
     * Crawl outwards operation to a specific room at the museum
     * @param thief Thief details
     * @param roomId
     * @return TRUE, if the room has not been reached yet. FALSE, if the room has been reaches
     * @throws InterruptedException
     */
    boolean crawlOut(ThiefDetails thief, int roomId) throws InterruptedException;

    /**
     * Crawl inwards operation to the collection site
     * @param thief Thief details
     * @param roomId
     * @return TRUE, if the collection site has not been reached yet. FALSE, if the collection site has been reaches
     * @throws InterruptedException
     */
    boolean crawlIn(ThiefDetails thief, int roomId) throws InterruptedException;


    /**
     * Roll a canvas operation. Thief steals a canvas, if available.
     * @param thief Thief details
     * @param roomId
     * @throws InterruptedException
     */
    void rollACanvas(ThiefDetails thief, int roomId) throws InterruptedException;

    /**
     * Thief reverse direction crawling towards the collection site
     * @param thief Thief details
     * @param roomId
     * @throws InterruptedException
     */
    void reverseDirection(ThiefDetails thief, int roomId) throws InterruptedException;

}
