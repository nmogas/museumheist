package org.mogas.museumheist.museum;

import org.mogas.museumheist.configurations.ApplicationSettings;
import org.mogas.museumheist.configurations.SettingsStrings;
import org.mogas.museumheist.logger.LogMessage;
import org.mogas.museumheist.logger.LogMessageType;
import org.mogas.museumheist.logger.Logger;
import org.mogas.museumheist.structures.MuseumCrawlParty;
import org.mogas.museumheist.structures.ThiefDetails;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nuno.silva on 18/02/2015.
 */
public class Museum implements IMuseumOrdinaryThief {

    private final int MAX_THIEVES_PARTY = (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.NUMBER_ELEMENTS_PER_GROUP.toString(), Integer.class);
    /**
     * Holds log reference
     */
    private Logger eventLog;

    private Map<Integer, MuseumCrawlParty> roomParty;

    /**
     * Array of rooms presents in the museum
     */
    private Room[] rooms;

    public Museum(Room[] rooms, Logger eventLog) {
        assert eventLog != null;
        this.eventLog = eventLog;
        this.rooms = rooms;
        this.roomParty = new HashMap<>();
        for (Room room : rooms) {
            roomParty.put(room.getROOM_ID(), new MuseumCrawlParty(eventLog, room.getDISTANCE()));
        }
    }

    @Override
    public synchronized boolean crawlOut(ThiefDetails thief, int roomId) throws InterruptedException {

        //tries to add the thief if he hadn't join the assault party yet! - and if he is the first - create a new party instance
        roomParty.get(roomId).addThief(thief);

        //if party isn't ready yet wait for its completion
        if (!roomParty.get(roomId).isDeployed()) {
            wait();
        }

        //assault party is ready! Notify the thief in the FIFO head that is his turn to crawl
        notifyAll();

        //if it is not this thief time to crawl - go sleep
        while (roomParty.get(roomId).getNextThiefIdToCrawl() != thief.getThiefId()) {
            wait();
        }

        //someone has crawled - not on first set anymore
        roomParty.get(roomId).setFirst(false);

        int jump = thief.getStrength();
        while (jump > 0) {
            if (roomParty.get(roomId).canCrawl(thief, jump)) {
                //update thief position
                thief.setCurrentPosition(jump);

                if (thief.getCurrentPosition() == rooms[roomId].getDISTANCE()){
                    jump = 0;
                    break;
                }
                //reset jump and try to do a big jump again
                jump = thief.getStrength();
                //log crawl

            } else {
                jump--;
            }
        }

        //if he reached the room attempt to crawl inwards
        if (thief.getCurrentPosition() == rooms[roomId].getDISTANCE()) {
            //try to crawl inwards and only after notify
            //reached the room!!
            return false;
        }

        //sets the next thief that needs to try to crawl
        roomParty.get(roomId).setNextThiefToCrawl(thief);
        //notify the next thief about his time to crawl
        notifyAll();

        //not reach the room - he can still crawl!
        return true;
    }

    @Override
    public synchronized boolean crawlIn(ThiefDetails thief, int roomId) throws InterruptedException {

        int jump = thief.getStrength();
        while (jump > 0) {
            if (roomParty.get(roomId).canCrawl(thief, jump)) {
                //update thief position
                thief.setCurrentPosition(jump);

                //reached coll site
                if (thief.getCurrentPosition() == rooms[roomId].getDISTANCE() * 2){
                    break;
                }
                //reset jump and try to do a big jump again
                jump = thief.getStrength();
            } else {
                jump--;
            }
        }

        //sets the next thief to crawl
        roomParty.get(roomId).setNextThiefToCrawl(thief);

        if (thief.getCurrentPosition() == rooms[roomId].getDISTANCE() * 2  ) {
            //remove himself from list
            roomParty.get(roomId).removeFromParty(thief);

            //notify other thieves about their turn to crawl
            notifyAll();

            //reached the collection site!!
            return false;
        }

        //notify the next thief about his turn to crawl
        notifyAll();

        //wait for his turn to crawl
        while (roomParty.get(roomId).getNextThiefIdToCrawl() != thief.getThiefId()) {
            wait();
        }

        return true;
    }


    @Override
    public synchronized void rollACanvas(ThiefDetails thief, int roomId) {
        thief.setHasCanvas(rooms[roomId].pickCanvas());
     }

    @Override
    public synchronized void reverseDirection(ThiefDetails thief, int roomId) throws InterruptedException {
        //change thief status
     }
}
