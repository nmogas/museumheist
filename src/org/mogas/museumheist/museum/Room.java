package org.mogas.museumheist.museum;

import org.mogas.museumheist.configurations.ApplicationSettings;
import org.mogas.museumheist.configurations.SettingsStrings;
import org.mogas.museumheist.logger.Logger;

import java.util.Random;

/**
 * Created by nuno.silva on 18/02/2015.
 */
public class Room{
    /**
     * Identifier of the room
     */
    private int roomId;

    /**
     * Holds log reference
     */
    private Logger eventLog;

    /**
     * Number of canvas that the room contains
     */
    private int nCanvas =  new Random().nextInt( (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.MAX_NUMBER_CANVAS.toString(), Integer.class) -
                                                 (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.MIN_NUMBER_CANVAS.toString(), Integer.class)  +
                                                 1) +
                                                 (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.MIN_NUMBER_CANVAS.toString(), Integer.class);

    /**
     * Distance to the room
     */
    private final int DISTANCE = new Random().nextInt( (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.MAX_ROOM_DISTANCE.toString(), Integer.class) -
                                                       (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.MIN_ROOM_DISTANCE.toString(), Integer.class)  +
                                                       1) +
                                                       (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.MIN_ROOM_DISTANCE.toString(), Integer.class);

    /**
     * Initialize a new Room object
     * @param roomId identifier of the room
     */
    public Room(int roomId, Logger eventLog){
        assert eventLog != null;
        this.roomId = roomId;
        this.eventLog = eventLog;
    }

    /**
     * Gets the room identifier
     * @return roomId
     */
    public int getROOM_ID() {
        return this.roomId;
    }

    /**
     * Gets the number of canvas that the room contains
     * @return N_CANVAS
     */
    public int getNCanvas() {
        return nCanvas;
    }

    public boolean pickCanvas(){
        if (nCanvas == 0){
            return false;
        }
        nCanvas--;
        return true;
    }

    /**
     * Gets the distance between the room entrance and the canvas
     * @return DISTANCE
     */
    public int getDISTANCE() {
        return DISTANCE;
    }


}
