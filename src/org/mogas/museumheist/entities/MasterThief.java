package org.mogas.museumheist.entities;

import org.mogas.museumheist.collectionsite.ICollectionMasterThief;
import org.mogas.museumheist.concentrationsite.IConcentrationMasterThief;
import org.mogas.museumheist.configurations.ApplicationSettings;
import org.mogas.museumheist.configurations.SettingsStrings;
import org.mogas.museumheist.logger.LogMessage;
import org.mogas.museumheist.logger.LogMessageType;
import org.mogas.museumheist.logger.Logger;
import org.mogas.museumheist.structures.AssaultParty;
import org.mogas.museumheist.structures.MasterThiefStates;
import org.mogas.museumheist.structures.ThiefDetails;

import java.util.*;

/**
 * Created by nuno.silva on 18/02/2015.
 */
public class MasterThief implements Runnable {

    private final int MAX_ACTIVE_PARTIES = ((Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.NUMBER_THIEVES.toString(), Integer.class) - 1) /
            ((Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.NUMBER_ELEMENTS_PER_GROUP.toString(), Integer.class));
    private final int N_ROOMS = (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.NUMBER_ROOMS.toString(), Integer.class);

    //holds log reference
    private Logger eventLog;
    /**
     * ConcentrationSite access monitor interaction
     *
     * @serialField collectionSite
     */
    private IConcentrationMasterThief concentrationSite;
    /**
     * CollectionSite access monitor interaction
     *
     * @serialField collectionSite
     */
    private ICollectionMasterThief collectionSite;
    /**
     * Current Master Thief life cycle state
     *
     * @serialField currentState
     */
    private MasterThiefStates currentState;
    /**
     * List of current active assault parties
     */
    private List<AssaultParty> activeAssaultPartyList;
    /**
     * Museum rooms ids to assault and their status
     */
    private Map<Integer, RoomStatus> targetRooms;
    /**
     * Number of canvas stolen from the museum
     */
    private int totalCanvas = 0;

    /**
     * Instantiation of the thread MasterThief
     */
    public MasterThief(Logger eventLog, IConcentrationMasterThief concentrationSite, ICollectionMasterThief collectionSite, int[] targetRooms) {
        assert concentrationSite != null;
        assert collectionSite != null;
        assert eventLog != null;
        assert targetRooms != null && targetRooms.length > 0;

        this.eventLog = eventLog;
        this.concentrationSite = concentrationSite;
        this.collectionSite = collectionSite;

        this.targetRooms = Collections.synchronizedMap(new HashMap<Integer, RoomStatus>(targetRooms.length));
        //set the rooms to assault and their initial status
        for (Integer room : targetRooms) {
            this.targetRooms.put(room, RoomStatus.UNKNOWN);
        }

        //set the master thief initial state
        this.currentState = MasterThiefStates.PLANNING_THE_HEIST;
    }

    @Override
    public void run() {
        boolean end = false;
        int eventClock;
        try {
            while (!end) {
                switch (currentState) {
                    case PLANNING_THE_HEIST:
                        eventLog.registerEvent(new LogMessage(LogMessageType.COLLECTION_SITE, null, -1, true, "STARTING OPERATIONS" ));
                        //initializes the heist
                        startOperations();

                        //check what to do next
                        currentState = MasterThiefStates.DECIDING_WHAT_TO_DO;
                        break;

                    case DECIDING_WHAT_TO_DO:
                        //evaluate the situation and decide what to do
                        eventLog.registerEvent(new LogMessage(LogMessageType.COLLECTION_SITE, null, -1, true, "APPRAISE SITUATION" ));
                        currentState = appraiseSit();
                        break;

                    case ASSEMBLING_A_GROUP:
                        //gets an available room to attack
                        int nextRoom = getNextTargetRoom();
                        assert nextRoom != -1;
                        eventLog.registerEvent(new LogMessage(LogMessageType.CONCENTRATION_SITE, null, nextRoom, true, "PREPARING ASSAULT TEAM" ));
                        AssaultParty party = concentrationSite.prepareAssaultParty(nextRoom);

                        //adds party to the list of active assault parties
                        activeAssaultPartyList.add(party);

                        //update the room status
                        targetRooms.put(party.getRoomId(), RoomStatus.UNDER_ATTACK);

                        eventLog.registerEvent(new LogMessage(LogMessageType.COLLECTION_SITE, null, party.getRoomId(), true, "SENDING ASSAULT TEAM" ));
                        collectionSite.sendAssaultParty(party);

                        //check what to do next
                        currentState = MasterThiefStates.DECIDING_WHAT_TO_DO;

                        break;

                    case WAITING_FOR_GROUP_ARRIVAL:

                        eventLog.registerEvent(new LogMessage(LogMessageType.COLLECTION_SITE, null, -1, true, "TAKING A REST" ));
                        ThiefDetails thief = collectionSite.takeARest();

                        AssaultParty thiefParty = getThiefAssaultParty(thief);
                        assert thiefParty != null;

                        int targetRoom = thiefParty.getRoomId();

                        eventLog.registerEvent(new LogMessage(LogMessageType.COLLECTION_SITE, null, -1, true, "COLLECT CANVAS" ));
                        boolean hasCanvas = collectionSite.collectCanvas(thief, targetRoom);

                        if (hasCanvas) totalCanvas++;

                        //remove thief from the assault party
                        thiefParty.deleteThief(thief);

                        //if party is empty remove from active parties
                        if (thiefParty.isEmpty()) {
                            //room still need more attacks until it is empty
                            if (hasCanvas) {
                                targetRooms.put(targetRoom, RoomStatus.NEED_ATTACK);
                            } else {
                                //room is now empty!
                                targetRooms.put(targetRoom, RoomStatus.EMPTY);
                            }
                            activeAssaultPartyList.remove(thiefParty);
                        }

                        //check what to do next
                        currentState = MasterThiefStates.DECIDING_WHAT_TO_DO;
                        break;

                    case PRESENTING_THE_REPORT:
                        eventLog.registerEvent(new LogMessage(LogMessageType.CONCENTRATION_SITE, null, -1, true, "PRESENTING REPORT" ));
                        concentrationSite.sumUpResults(totalCanvas);

                        //finish the simulation
                        end = true;
                        break;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void startOperations() {
        //assault first room to start
        activeAssaultPartyList = Collections.synchronizedList(new ArrayList<AssaultParty>());
    }


    private MasterThiefStates appraiseSit() {
        //number of rooms being assaulted
        int nRoomsUnderAttack = activeAssaultPartyList.size();

        //number of empty rooms
        int nEmptyRooms = getNumberEmptyRooms();

        //number of rooms not empty
        int nRoomsNotEmpty = N_ROOMS - nEmptyRooms;

        //check if rooms are all empty
        boolean allRoomsEmpty = nEmptyRooms == N_ROOMS;

        MasterThiefStates status;


        if (nEmptyRooms < N_ROOMS) {
            if (MAX_ACTIVE_PARTIES - nRoomsUnderAttack > 0 && (nRoomsUnderAttack + nEmptyRooms) < N_ROOMS) {
                status = MasterThiefStates.ASSEMBLING_A_GROUP;
            } else {
                status = MasterThiefStates.WAITING_FOR_GROUP_ARRIVAL;
            }
        } else {
            if (MAX_ACTIVE_PARTIES - nRoomsUnderAttack < MAX_ACTIVE_PARTIES || nEmptyRooms == nRoomsUnderAttack) {
                status = MasterThiefStates.WAITING_FOR_GROUP_ARRIVAL;
            } else {
                status = MasterThiefStates.PRESENTING_THE_REPORT;
            }
        }

        return status;
    }

    private boolean isValidNewParty(AssaultParty assaultParty) {
        assert assaultParty != null;

        if (activeAssaultPartyList.isEmpty()) {
            return true;
        }

        for (AssaultParty party : activeAssaultPartyList) {
            if (party.compareTo(assaultParty) == 1 || party.containsThief(assaultParty.getThieves())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Gets a target room that is neither empty or under attack
     *
     * @return id of the room
     */
    private int getNextTargetRoom() {
        for (Map.Entry<Integer, RoomStatus> entry : targetRooms.entrySet()) {
            if (((RoomStatus) entry.getValue()) == RoomStatus.UNKNOWN || (((RoomStatus) entry.getValue()) == RoomStatus.NEED_ATTACK)) {
                return entry.getKey();
            }
        }
        return -1;
    }

    private int getNumberEmptyRooms() {
        int count = 0;
        for (RoomStatus status : targetRooms.values()) {
            if (status == RoomStatus.EMPTY) {
                count++;
            }
        }
        return count;
    }

    private AssaultParty getThiefAssaultParty(ThiefDetails thief) {

        for (AssaultParty party : activeAssaultPartyList) {
            if (party.thiefExists(thief)) {
                return party;
            }
        }
        return null;
    }
    private enum RoomStatus {
        /**
         * Initial room status
         */
        UNKNOWN,

        /**
         * Room currently being attacked
         */
        UNDER_ATTACK,

        /**
         * Renew attack to the room
         */
        NEED_ATTACK,

        /**
         * Room is empty
         */
        EMPTY
    }
}
