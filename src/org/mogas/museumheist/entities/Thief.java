package org.mogas.museumheist.entities;

import org.mogas.museumheist.collectionsite.ICollectionOrdinaryThief;
import org.mogas.museumheist.concentrationsite.IConcentrationOrdinaryThief;
import org.mogas.museumheist.configurations.ApplicationSettings;
import org.mogas.museumheist.configurations.SettingsStrings;
import org.mogas.museumheist.logger.*;
import org.mogas.museumheist.museum.IMuseumOrdinaryThief;
import org.mogas.museumheist.structures.ThiefDetails;

/**
 * Created by nuno.silva on 18/02/2015.
 */
public class Thief implements Runnable {

    private enum ThiefStates {
        OUTSIDE,

        CRAWLING_OUTWARDS,

        AT_A_ROOM,

        CRAWLING_INWARDS
    }

    private final int STRENGTH = 1 + (int) (Math.random() * (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.MAX_THIEF_STRENGTH.toString(), Integer.class));

    //Holds log reference
    private Logger eventLog;

    /**
     * CollectionSite access monitor interaction
     *
     * @serialField collectionSite
     */
    private ICollectionOrdinaryThief collectionSite;

    /**
     * ConcentrationSite access monitor interaction
     *
     * @serialField collectionSite
     */
    private IConcentrationOrdinaryThief concentrationSite;

    /**
     * Museum access monitor interaction
     *
     * @serialField collectionSite
     */
    private IMuseumOrdinaryThief museum;

    /**
     * Current thief life cycle state
     *
     * @serialField currentState
     */
    private ThiefStates currentState;

    /**
     * Thief Characteristics
     */
    private ThiefDetails thiefDetails;

    /**
     * Instantiation of the thread Thief
     */
    public Thief(int thiefId, Logger eventLog, IConcentrationOrdinaryThief concentrationSite, ICollectionOrdinaryThief collectionSite, IMuseumOrdinaryThief museum) {
        assert eventLog != null;
        assert concentrationSite != null;
        assert collectionSite != null;
        assert museum != null;

        this.eventLog = eventLog;
        this.thiefDetails = new ThiefDetails(thiefId, STRENGTH);
        this.concentrationSite = concentrationSite;
        this.collectionSite = collectionSite;
        this.museum = museum;

        //sets the initial state
        this.currentState = ThiefStates.OUTSIDE;
    }

    /*
    * Thieves life-cycle
    */
    @Override
    public void run() {
        //flag that says if the simulation is about to end
        boolean end = false;

        //flag that says if the thief can crawl
        boolean canCrawl =  true;

        //flag that says if the thief carries any canvas
        boolean hasCanvas = false;

        //current target room of the thief
        int roomId = -1;

        //logical clock current value
        int eventClock;
        try {
            while (!end) {
                switch (currentState) {
                    case OUTSIDE:
                        eventLog.registerEvent(new LogMessage(LogMessageType.CONCENTRATION_SITE, thiefDetails, -1, false, "AM I NEEDED" ));
                        boolean isNeeded = concentrationSite.amINeeded(this.thiefDetails);

                        //thief is not needed anymore, simulation will end
                        if (!isNeeded){
                            end = true;
                            break;
                        }
                        eventLog.registerEvent(new LogMessage(LogMessageType.COLLECTION_SITE, thiefDetails, -1, false, "WAITING FOR PARTY COMPLETION" ));
                        roomId = collectionSite.prepareExcursion(this.thiefDetails);

                        assert roomId != -1;

                        //party is ready, start crawling to the museum
                        currentState = ThiefStates.CRAWLING_OUTWARDS;
                        break;

                    case CRAWLING_OUTWARDS:
                        //crawl towards the museum until the room is reached
                        canCrawl = true;
                        do{
                            eventLog.registerEvent(new LogMessage(LogMessageType.MUSEUM, thiefDetails, roomId, false, "CRAWLING OUTWARDS" ));
                            canCrawl = museum.crawlOut(this.thiefDetails, roomId);
                        }while(canCrawl);

                        //reaches the room to steal a canvas
                        currentState = ThiefStates.AT_A_ROOM;
                        break;

                    case AT_A_ROOM:
                        eventLog.registerEvent(new LogMessage(LogMessageType.ROOM, thiefDetails, roomId, false, "STEALING CANVAS" ));
                        //steal a canvas from the museum
                        museum.rollACanvas(this.thiefDetails, roomId);

                        //reverse direction();
                        museum.reverseDirection(this.thiefDetails, roomId);

                        //thief state changes to crawl in
                        currentState = ThiefStates.CRAWLING_INWARDS;
                        break;

                    case CRAWLING_INWARDS:
                        //crawl towards the collection site until it is reached
                        canCrawl = true;
                        do {
                            eventLog.registerEvent(new LogMessage(LogMessageType.MUSEUM, thiefDetails, roomId, false, "CRAWLING INWARDS" ));
                            canCrawl = museum.crawlIn(this.thiefDetails, roomId);
                        } while(canCrawl);

                        eventLog.registerEvent(new LogMessage(LogMessageType.MUSEUM, thiefDetails, roomId, false, "DELIVERING CANVAS" ));
                        //thief reaches the collection site and hand a canvas
                        collectionSite.handACanvas(this.thiefDetails);

                        //waits for new orders
                        currentState = ThiefStates.OUTSIDE;
                        break;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
