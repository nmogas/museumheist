package org.mogas.museumheist.logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nuno.silva on 23/02/2015.
 */
public class ControlledHashMap<T> {

    /**
     * Map used to save the values
     */
    private Map<Integer, T> map;
    /**
     * Last key of value read
     */
    private int lastRead;
    /**
     * Last key of value available
     */
    private int lastAvailable;

    /**
     * Constructor of this class
     */
    public ControlledHashMap() {
        map = new HashMap<Integer, T>();
        lastRead = -1;
        lastAvailable = -1;
    }

    /**
     * Method that allow read next value if next key is available
     *
     * @return <li> <b>null</b> if the next value isn't available
     *         <li> <b>the value</b> if next value is available
     */
    public T getNext() {
        T tmp;

        if( lastRead == lastAvailable ) {
            return null;
        } else {
            lastRead++;
            tmp = map.get(lastRead);
            return tmp;
        }
    }

    /**
     * Method that allow insert a new value in map
     *
     * @param key key of value to insert
     * @param value value to insert
     */
    public void put(Integer key, T value) {
        map.put(key, value);

        while(map.containsKey(lastAvailable+1)) {
            lastAvailable++;
        }
    }

    public Map<Integer, T> getMap() {
        return map;
    }
}
