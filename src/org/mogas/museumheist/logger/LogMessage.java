package org.mogas.museumheist.logger;

import org.mogas.museumheist.structures.ThiefDetails;

/**
 * Created by nuno.silva on 23/02/2015.
 */
public class LogMessage{

    private LogMessageType type;

    private int clock;

    private ThiefDetails thief;

    private int roomId;

    private boolean isMasterThief;

    private String nextState;

    public LogMessage(LogMessageType type, ThiefDetails thief, int roomId, boolean isMasterThief, String state) {
        this.type = type;
        this.thief = thief;
        this.clock = LogicalClock.getInstance().getNextClock();
        this.roomId = roomId;
        this.isMasterThief = isMasterThief;
        this.nextState = state;
    }

    public LogMessageType getType() {
        return type;
    }

    public int getClock() {
        return clock;
    }

    public ThiefDetails getThief() {
        return thief;
    }

    public int getRoomId() {
        return roomId;
    }

    public boolean isMasterThief() {
        return isMasterThief;
    }

    public String getNextState() {
        return nextState;
    }
}
