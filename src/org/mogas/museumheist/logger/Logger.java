package org.mogas.museumheist.logger;

import org.mogas.museumheist.configurations.LogStrings;
import sun.rmi.runtime.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by nuno.silva on 23/02/2015.
 */
public class Logger {

    private final static String logFilePath = "logger";
    private final static String logFileName = "log.txt";
    private final static String separator = System.getProperty("file.separator");
    private final static String fullPath = logFilePath + separator + logFileName;
    private ControlledHashMap<LogMessage> eventsMap;
    private int n_thieves;
    private int n_rooms;


    public Logger(int n_thieves, int n_rooms) {
        this.n_thieves = n_thieves;
        this.n_rooms = n_rooms;
        initDefaults();
    }

    private void initDefaults() {
        eventsMap = new ControlledHashMap<LogMessage>();

        //create dir if not exists
        //check if directory exists, if not create it
        File dir = new File(logFilePath);
        if (!dir.exists()) {
            dir.mkdir();
        }

        //delete log file if exists
        try {
            Files.deleteIfExists(Paths.get(fullPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        createHeaders();
    }

    public synchronized void registerEvent(LogMessage message) {
        assert message != null;
        eventsMap.put(message.getClock(), message);
        processEvent();
    }

    public void processRoomEvents() {
        Map<Integer, List<LogMessage>> roomMessages = new HashMap<>();
        for (LogMessage message : eventsMap.getMap().values()) {
            if (!roomMessages.containsKey(message.getRoomId())) {
                List<LogMessage> list = new ArrayList<>();
                list.add(message);
                roomMessages.put(message.getRoomId(), list);
            } else {
                roomMessages.get(message.getRoomId()).add(message);
            }
        }

        for (Map.Entry<Integer, List<LogMessage>> entry : roomMessages.entrySet()) {
            //sort messages by time
            Collections.sort(entry.getValue(), (LogMessage l1, LogMessage l2) -> {
                return l1.getClock() - l2.getClock();
            });

            entry.getValue().stream().forEach(logMessage -> {
                try (FileOutputStream fos = new FileOutputStream(logFilePath + separator + logFileName, true)) {

                    //process message
                    fos.write(processLine(logMessage));

                    } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            });
        }
    }

    private synchronized void processEvent() {
        LogMessage logMessage = eventsMap.getNext();
        if (logMessage != null) {
            try (FileOutputStream fos = new FileOutputStream(logFilePath + separator + logFileName, true)) {

                //process message
                fos.write(processLine(logMessage));

            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }


    private byte[] processLine(LogMessage logMessage){

        StringBuffer sb = new StringBuffer();

        switch (logMessage.getType()){
            case CONCENTRATION_SITE:
                break;
            case COLLECTION_SITE:
                break;
            case MUSEUM:
                break;
            case ROOM:
                break;
        }

        return sb.toString().getBytes();
    }

    private void createHeaders() {
        StringBuilder header = new StringBuilder();

        //open stream to write on file
        try (FileOutputStream fos = new FileOutputStream(logFilePath + separator + logFileName, true)) {

            //write heading
            fos.write(LogStrings.HEADER.getBytes());

            int mtSize;
            header.append("  " + LogStrings.MASTER_THIEF);
            header.append(" |");
            mtSize = header.length();
            //n thieves
            for (int i = 0; i < n_thieves - 1; i++) {
                header.append("  " + getThiefNumberHeader(i) + " |");
            }
            //n_rooms
            for (int i = 0; i < n_rooms; i++) {
                header.append("\t" + LogStrings.ROOM + "_" + i + "\t|");
            }

            //write heading
            fos.write(header.toString().getBytes());

            //sub heading
            String tStatusHeader = getThiefStatusHeader();
            String rStatusHeader = getRoomStatusHeader();
            header = new StringBuilder("\n");

            //state string
            int nSpaces = (mtSize - LogStrings.STATE.length()) / 2 ;
            String whiteSpaces = getWhitePaceString(nSpaces);
            header.append(whiteSpaces);
            header.append(LogStrings.STATE);
            header.append(whiteSpaces);

            header.append("|");

            //thieves info string
            for (int i = 0; i < n_thieves - 1; i++) {
                header.append(" " + tStatusHeader + " |");
            }

            //room status string
            for (int i = 0; i < n_rooms ; i++) {
                header.append("  " + rStatusHeader + "  |");
            }

            //n total canvas stolen
            header.append("\t" + LogStrings.TOTAL_CANVAS + "\t|");

            //write initial status Header
            fos.write(header.toString().getBytes());

            int size = header.length();
            header = new StringBuilder("\n");
            //dumb characters
            for(int i = 0; i < size; i++){
                header.append("-");
            }
            //write dumb characters
            fos.write(header.toString().getBytes());
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private String getThiefStatusHeader(){
        return  LogStrings.STRENGTH + "  " + LogStrings.ON_PARTY + "  " + LogStrings.TARGET_ROOM + "  " + LogStrings.POSITION + "  " + LogStrings.HAS_CANVAS;
    }

    private String getThiefNumberHeader(int n){
        int statusSize = getThiefStatusHeader().length();
        String tNumber = LogStrings.THIEF + "_" + n;
        int difference = (statusSize - tNumber.length()) / 2;

        return getWhitePaceString(difference) + tNumber + getWhitePaceString(difference);
    }

    private String getRoomStatusHeader(){
        return LogStrings.ROOM_DISTANCE + " " + LogStrings.ROOM_N_CANVAS;
    }

    private String getRoomStatus(int n){
        int statusSize = (LogStrings.ROOM + "_" + n).length();
        String rNumber = getRoomStatusHeader();
        int difference = (statusSize - rNumber.length()) / 2;
        return getWhitePaceString(difference) + rNumber + getWhitePaceString(difference);
    }

    private String getWhitePaceString(int nSpaces) {
        char[] bytes = new char[nSpaces];
        Arrays.fill(bytes, ' ');
        return new String(bytes);
    }
}
