package org.mogas.museumheist.logger;

/**
 * Created by nuno.silva on 23/02/2015.
 */
public class ActionMessage {

    /** Master Thief Messages **/
    public final static String APPRAISE = "APPRAISE_SITUATION";

    public final static String PREPARE_TEAM = "PREPARE_ASSAULT_TEAM";

    public final static String SEND_TEAM = "SEND_ASSAULT TEAM";

    public final static String TAKING_REST = "TAKING_REST";

    public final static String COLLECTING_CANVAS = "COLLECTING_CANVAS";

    public final static String SUM_RES = "SUM_RESULTS";

    /** Ordinary Thief Messages **/
    public final static String AM_I_NEEDED = "AM_I_NEEDED";

    public final static String PREPARE_EXCURSION = "PREPARE_EXCURSION";

    public final static String CRAWL_OUT = "CRAWL_OUT";

    public final static String ROLL_CANVAS = "ROLL_CANVAS";

    public final static String REVERSE_DIRECTION = "REVERSE_DIRECTION";

    public final static String CRAWL_IN = "CRAWL_IN";

    public final static String HAND_CANVAS = "HAND_CANVAS";
}
