package org.mogas.museumheist.logger;

import java.util.Locale;

/**
 * Created by nuno.silva on 23/02/2015.
 */
public class LogicalClock {

    private static volatile LogicalClock instance = null;

    //clock event time
    private int clock;

    public static LogicalClock getInstance(){
        if (instance == null){
            synchronized (LogicalClock.class){
                if (instance == null){
                    instance = new LogicalClock();
                }
            }
        }
        return instance;
    }

    private LogicalClock(){
        //init defaults
        clock = 0;
    }

    public synchronized int getNextClock(){
        return clock++;
    }

}
