package org.mogas.museumheist.collectionsite;

import org.mogas.museumheist.structures.ThiefDetails;

/**
 * Created by nuno.silva on 18/02/2015.
 */
public interface ICollectionOrdinaryThief {

    /**
     * Prepare for the excursion operation - joins the assault party he has been assigned to and waits for sendAssaultTeam()
     * @param thief details
     * @throws InterruptedException
     */
    int prepareExcursion(ThiefDetails thief) throws InterruptedException;

    /**
     * Notifies master thief that thief has arrived
     * @param thief Thief details
     * @throws InterruptedException
     */
    void handACanvas(ThiefDetails thief) throws InterruptedException;

}
