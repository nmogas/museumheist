package org.mogas.museumheist.collectionsite;

import org.mogas.museumheist.structures.AssaultParty;
import org.mogas.museumheist.structures.ThiefDetails;

import java.util.List;

/**
 * Created by nuno.silva on 18/02/2015.
 */
public interface ICollectionMasterThief {


    /**
     *
     * @param assaultParty Assault party to be deployed

     * @throws InterruptedException
     */
    void sendAssaultParty(AssaultParty assaultParty) throws InterruptedException;

    /**
     *
     * @return Details of the ordinary thief who just arrived
     * @throws InterruptedException
     */
    ThiefDetails takeARest() throws InterruptedException;

    /**
     * 0rdinary thief is released from his duties for now
     * @param thief details of the ordinary thief who just arrived to deliver the canvas
     * @return True if thief carries a canvas, False if brings no canvas (room empty)
     * @throws InterruptedException
     */
    boolean collectCanvas(ThiefDetails thief, int roomId) throws InterruptedException;
}
