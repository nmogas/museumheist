package org.mogas.museumheist.collectionsite;

import org.mogas.museumheist.configurations.ApplicationSettings;
import org.mogas.museumheist.configurations.SettingsStrings;
import org.mogas.museumheist.entities.Thief;
import org.mogas.museumheist.logger.ActionMessage;
import org.mogas.museumheist.logger.LogMessage;
import org.mogas.museumheist.logger.LogMessageType;
import org.mogas.museumheist.logger.Logger;
import org.mogas.museumheist.structures.AssaultParty;
import org.mogas.museumheist.structures.ThiefDetails;

import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;


/**
 * Created by nuno.silva on 18/02/2015.
 */
public class CollectionSite implements ICollectionMasterThief, ICollectionOrdinaryThief {

    //holds log reference
    private Logger eventLog;

    private List<Integer> thiefForExcursion;
    private Map<Integer, Integer> thiefRoomMap;
    private Queue<ThiefDetails> backFromMuseumThief;

    private final int MAX_ELEMENTS_PARTY = (Integer) ApplicationSettings.getInstance().getProperty(SettingsStrings.NUMBER_ELEMENTS_PER_GROUP.toString(), Integer.class);

    public CollectionSite(Logger eventLog) {
        this.eventLog = eventLog;
        this.thiefForExcursion = Collections.synchronizedList(new ArrayList<Integer>());
        this.thiefRoomMap = new HashMap<>();
        this.backFromMuseumThief = new PriorityBlockingQueue<>();
    }

    @Override
    public synchronized void sendAssaultParty(AssaultParty assaultParty) throws InterruptedException {
        assert assaultParty != null;

        //wait for all member to arrive the collection site
        for(ThiefDetails thief : assaultParty.getThieves()){
            assert !thiefRoomMap.containsKey((Object)thief.getThiefId());
            while(!thiefForExcursion.contains(thief.getThiefId())){
                wait();
            }
        }


        //remove thieves for excursion  list and assign them a team
        for(ThiefDetails thief : assaultParty.getThieves()){
            thiefForExcursion.remove((Object)thief.getThiefId());
            thiefRoomMap.put(thief.getThiefId(), assaultParty.getRoomId());
        }

        //all thieves are waiting - the party is now ready, notify them
        //notify all
        notifyAll();
    }

    @Override
    public synchronized ThiefDetails takeARest() throws InterruptedException {

        while(backFromMuseumThief.isEmpty()){
            wait();
        }

        return backFromMuseumThief.poll();
    }

    @Override
    public synchronized boolean collectCanvas(ThiefDetails thief, int roomId) throws InterruptedException {

        //remove thief from the assault party
        thiefRoomMap.remove((Object)thief.getThiefId());

        //notify thief and collect canvas
        notifyAll();

        return thief.hasCanvas();
    }

    @Override
    public synchronized int prepareExcursion(ThiefDetails thief) throws InterruptedException {

        //add thief to the list of arrived thieves!
        assert !thiefForExcursion.contains(thief.getThiefId());
        thiefForExcursion.add(thief.getThiefId());

        //notify master thief of his arrival
        notifyAll();

        //wait for team to be ready
        while(!thiefRoomMap.containsKey(thief.getThiefId())){
            wait();
        }

        //team was deployed!
        return thiefRoomMap.get(thief.getThiefId());
    }

    @Override
    public synchronized void handACanvas(ThiefDetails thief) throws InterruptedException {

        //add to the list of arrived thieves
        backFromMuseumThief.add(thief);

        //notify master of his arrival
        notifyAll();

        //wait for master thief to collect/or not  his canvas
        while(thiefRoomMap.containsKey((Object)thief.getThiefId())){
            wait();
        }

        //canvas delivered
        thief.setHasCanvas(false);
    }
}